// Operators

// Assignment operators
// Basic assignment operators (=)
let assignmentNumber = 8;

//Arithmetic Assignment Operators
// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator : " + assignmentNumber);

// Shortcut
assignmentNumber += 2;
console.log("Result of addition assignment operator : " + assignmentNumber);

// Subtraction assignment operator (-=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator : " + assignmentNumber);

// Multiplication assignemnt operator (*=)
assignmentNumber *= 2;
console.log("Result of subtraction assignment operator : " + assignmentNumber);

// Division assignemnt operator (*=)
assignmentNumber /= 2;
console.log("Result of division assignment operator : " + assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator:" + sum);

let diff = x - y;
console.log("Result of subtraction operator:" + diff);

let prod = x * y;
console.log("Result of subtraction operator:" + prod);

let quot = x / y;
console.log("Result of quotient operator:" + quot);

let remainder = x % y
console.log("Result of remainder operator:" + remainder);

// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas)

// Using Parentheses
let pemdas = 1 + (2 -3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment(++) and Decrement (--)
let z = 1;
// Pre-increment -add value before 
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-increment: " + z);

// Post-decrement
decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);
// typeof kung ano data type ng spcific var

let numC = true + 1;
console.log(numC);
console.log(typeof numC);

let numD = false + 1;
console.log(numD);

// Relational operators

// Quality operator (==)
let juan = 'juana';
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log('juan' == juan);

// Inequality operator(!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log('juan' != juan);

// <,>,<=,>= 
console.log("LEss than and greater than")
console.log(4 < 6);
console.log(2 > 8);
console.log(5 >= 5);
console.log(10 <= 15);

// strict Equality Operator (===)
console.log("Strict equality:");
console.log(1 === '1');

// strict inequality operator(!==);
console.log("Strict inequality:");
console.log(1 !== '1');

// Logical operators
let isLegalAge = true;
let isRegistered = false;

// And Operator(&&// t = t and t)
// Rturns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Results of logical AND operator: " + allRequirementsMet);

// Or Operator(||)
// Return true if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection control structures

// if, else if and else statement

// if statement
// Executes a statement if a specified condition is true
/* Syntax:
	if(condition){
		statement/s;
	}
*/

let numE = -1;

if(numE < 0){
	console.log("Hello");
}

let nickName = 'Mattheo';

if(nickName === 'Matt'){
	console.log("Hello " + nickName);
}

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true
/* Syntax
	if(condition1){
		statement/s;
	}
	else if(condition2){
		statment/s;
	}
	else if(condition3){
		statment/s;
	}
	else if(conditionN){
		statment/s;
	}

*/

let numF = 1;

if(numE > 0){
	console.log("Hello");
}
else if(numF > 0){
	console.log("World");
}

// else clause
// executes a statement if all other conditions are not met.
/* Syntax:
	if(condition1){
		statement/s;
	}
	else if(condition2){
		statment/s;
	}
	else if(condition3){
		statment/s;
	}
	else if(conditionN){
		statment/s;
	}
	else{
		statement/s;
	}
*/

/*
	numE = -1;
	numF = 1;
*/
if(numE > 0){
	console.log("Hello");
}
else if(numF === 0){
	console.log("World");
}
else{
	console.log("Again");
}

let message = 'No message';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.';
	}
	else if(windSpeed >= 31 && windSpeed <= 60){
		return 'Tropical depression detected';
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	}
	else if(windSpeed >= 89 && windSpeed <=117){
		return 'Severe tropical strom detected';
	}
	else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

let department = prompt('What departmernt are you?');

if(department === 'a' || department === 'A'){
	console.log("You are in department A");
}

else if(department === 'b' || department === 'B'){
	console.log("You are in department B");
}
else{
	console.log("Invalid department");
}


// Ternary operator
/* Syntax:
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? "true" : "false";
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// parseInt converts string values into numeric types
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name);

// Switch statement
/*		syntax:
		switch(expression){
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			case valuen:
					statement;
					break;
			default:
				statement;
		}
*/

let day = prompt("What day of the week is today?").toLowerCase();
console.log(day);

switch(day){
	case 'sunday':
		console.log("the color of the day is red");
		break;
	case 'monday':
		console.log("the color of the day is orange");
		break;
	case 'tuesday':
		console.log("the color of the day is yellow");
		break;
	case 'wednesday':
		console.log("the color of the day is green");
		break;
	case 'thursday':
		console.log("the color of the day is blue");
		break;
	case 'friday':
			console.log("the color of the day is indigo");
		break;
	case 'saturday':
			console.log("the color of the day is violet");
		break;
	default:
	console.log("Please input a valid day");
};


// Try-catch-Finally statement
// For error handling / hindi ginagamit to be part of the codebase


function showIntensityAlert(windSpeed){
	try{
		// attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	// error/err are commonly used var by develppers for storing errors
	catch (error){
		console.log(typeof error);

		console.warn(error.message);
		// error.message is to access information relating to an error object
	}

	finally{
		// continue on executing the code regardless of success or failure of code execution in the "try" block.
		alert('Intensity updates will show new alert.')
		}
}

showIntensityAlert(56);